import configparser
import sys
import os
import threading
import tkinter as tk
from tkinter import filedialog

from PyPDF2 import PdfFileMerger


def read_configuration():
    config_file = os.path.join(os.path.expanduser('~'), '.pdfmerger.ini')
    config = configparser.ConfigParser()
    config.read(config_file)
    return config

def write_configuration(preferences):
    config_file = os.path.join(os.path.expanduser('~'), '.pdfmerger.ini')
    with open(config_file, 'w') as configfile:
        preferences.write(configfile)

    if sys.platform.startswith('win32'):
        os.system(f'attrib +h {config_file}')


class ListFrame(tk.Frame):
    def __init__(self, master):
        super().__init__(master)

        frame = tk.Frame(self)

        self.values = tk.Variable()
        self.list = DragDropListbox(frame, width=50, listvariable=self.values)
        self.vscroll = tk.Scrollbar(self, orient=tk.VERTICAL, command=self.list.yview)
        self.hscroll = tk.Scrollbar(frame, orient=tk.HORIZONTAL, command=self.list.xview)
        self.list.config(xscrollcommand=self.hscroll.set)
        self.list.config(yscrollcommand=self.vscroll.set)

        self.list.pack(side=tk.TOP)
        self.hscroll.pack(side=tk.BOTTOM, fill=tk.X)
        frame.pack(side=tk.LEFT)
        self.vscroll.pack(side=tk.RIGHT, fill=tk.Y)

    def get_list(self):
        return self.values.get()

    def pop_selection(self):
        index = self.list.curselection()
        if index:
            value = self.list.get(index)
            self.list.delete(index)
            return value

    def clear(self):
        self.delete(0, tk.END)

    def delete(self, *args):
        self.list.delete(*args)

    def insert(self, i, *items):
        self.list.insert(i, *items)

    def push(self, item):
        self.list.insert(tk.END, item)

    def empty(self):
        return len(self.get_list()) == 0


class DragDropListbox(tk.Listbox):
    """ A Tkinter listbox with drag'n'drop reordering of entries. """
    def __init__(self, master, **kw):
        kw['selectmode'] = tk.SINGLE
        tk.Listbox.__init__(self, master, kw)
        self.bind('<Button-1>', self.setCurrent)
        self.bind('<B1-Motion>', self.shiftSelection)
        self.curIndex = None

    def setCurrent(self, event):
        self.curIndex = self.nearest(event.y)

    def shiftSelection(self, event):
        i = self.nearest(event.y)
        if i < self.curIndex:
            x = self.get(i)
            self.delete(i)
            self.insert(i+1, x)
            self.curIndex = i
        elif i > self.curIndex:
            x = self.get(i)
            self.delete(i)
            self.insert(i-1, x)
            self.curIndex = i


class App(tk.Tk):
    def __init__(self):
        super().__init__()

        self.preferences = read_configuration()

        self.input_directory = self.preferences['DEFAULT'].get('InputDirectory', "~")
        self.output_directory = self.preferences['DEFAULT'].get("OutputDirectory", "~")

        file_entries_frame = tk.Frame()

        self.file_entries_group = tk.LabelFrame(
            file_entries_frame,
            padx=5, pady=10,
            text="Archivos a unir"
        )
        self.file_entries_group.pack(padx=12, pady=(12, 0))

        prefix_frame = tk.Frame(
            self.file_entries_group,
            padx=6, pady=6
        )
        prefix_lbl = tk.Label(
            prefix_frame,
            text="Prefijo: "
        ).pack(side=tk.LEFT)
        self.prefix_var = tk.StringVar()
        self.prefix_var.trace('w', self.trace_prefix_entry)
        self.prefix_entry = tk.Entry(
            prefix_frame,
            textvariable=self.prefix_var,
            width=30
        )
        self.prefix_entry.pack(side=tk.LEFT)

        prefix_frame.pack(side=tk.TOP)

        list_frame = tk.Frame(
            self.file_entries_group
        )

        self.list_files = ListFrame(list_frame)
        self.move_right_btn = tk.Button(list_frame, text=">", command=self.move_right)
        self.move_left_btn = tk.Button(list_frame, text="<", command=self.move_left)
        self.selected_files_list = ListFrame(list_frame)

        self.list_files.pack(side=tk.LEFT, padx=10, pady=10)
        self.selected_files_list.pack(side=tk.RIGHT, padx=10, pady=10)
        self.move_right_btn.pack(expand=True, ipadx=5)
        self.move_left_btn.pack(expand=True, ipadx=5)

        list_frame.pack(side=tk.BOTTOM)

        file_entries_frame.pack(side=tk.TOP)

        directories_frame = tk.Frame()

        # Input config
        self.input_ = tk.Button(
            directories_frame,
            text=f"Carpeta de entrada: {os.path.expanduser(self.input_directory)}",
            command=self.pick_input_directory
        )
        self.input_.pack(side=tk.LEFT)

        tk.Label(directories_frame, text="➔").pack(side=tk.LEFT)

        self.output = tk.Button(
            directories_frame,
            text=f"Carpeta de salida: {os.path.expanduser(self.output_directory)}",
            command=self.pick_output_directory
        )
        self.output.pack(side=tk.LEFT)

        self.output_filename = tk.Entry(directories_frame, text="unido.pdf")
        self.output_filename.insert(0, "unido.pdf")
        self.output_filename.pack(side=tk.BOTTOM)

        directories_frame.pack(side=tk.TOP)

        actions_frame = tk.Frame()

        # # Action buttons
        self.merge_pdf_btn = tk.Button(
            actions_frame,
            text="Unir archivos",
            command=self.process_pdf_files,
            state=tk.DISABLED
        )
        self.merge_pdf_btn.pack(side=tk.RIGHT)

        self.reset_btn = tk.Button(
            actions_frame,
            text="Volver a empezar",
            command=self.reset_entries
        )
        self.reset_btn.pack(side=tk.RIGHT)

        actions_frame.pack()

        self.global_status_lbl = tk.Label(self)
        self.global_status_lbl.pack(side=tk.TOP)

        self.found_files = []

        self.prefix_entry.focus_set()

    def move_right(self):
        self.move(self.list_files, self.selected_files_list)

    def move_left(self):
        self.move(self.selected_files_list, self.list_files)

    def move(self, from_, to_):
        value = from_.pop_selection()
        if value:
            to_.push(value)

        if self.selected_files_list.empty():
            self.merge_pdf_btn.config(state=tk.DISABLED)
        else:
            self.merge_pdf_btn.config(state=tk.NORMAL)

    def trace_prefix_entry(self, *args):
        self.trigger_search_by_prefix()

    def pick_input_directory(self):
        self.input_directory = filedialog.askdirectory(
            initialdir="~",
            title="Elige el directorio de entrada"
        )
        self.input_.configure(text=f"Carpeta de entrada: {os.path.expanduser(self.input_directory)}")
        self.preferences['DEFAULT']['InputDirectory'] = os.path.expanduser(self.input_directory)
        write_configuration(self.preferences)
        self.focus_set()
        self.prefix_entry.focus_set()

        self.trigger_search_by_prefix()

    def get_prefix(self):
        return self.prefix_entry.get()

    def trigger_search_by_prefix(self):
        prefix = self.get_prefix()
        self.config(cursor="watch")
        self.global_status_lbl.configure(text="Buscando...")
        thread = threading.Thread(target=self.search_by_prefix)
        thread.start()
        self.display_message_on_success(thread, "Búsqueda completa", prefix)
        self.show_files_in_list(thread)

    def show_files_in_list(self, thread):
        if thread.is_alive():
            self.after(100, lambda: self.show_files_in_list(thread))
        else:
            self.list_files.clear()
            self.list_files.insert(0, *self.found_files)

    def display_message_on_success(self, thread, message, prefix, timeout=3000):
        if thread.is_alive():
            self.after(100, lambda: self.display_message_on_success(thread, message, prefix, timeout))
        else:
            # display message only if current prefix is thread's prefix
            if self.get_prefix() == prefix:
                self.config(cursor="arrow")
                self.global_status_lbl.configure(text=message)
                self.after(timeout, lambda: self.global_status_lbl.configure(text=""))

    def pick_output_directory(self):
        self.output_directory = filedialog.askdirectory(
            initialdir="~",
            title="Elige el directorio de salida"
        )
        self.output.configure(text=f"Directorio de salida: {os.path.expanduser(self.output_directory)}")
        self.preferences['DEFAULT']['OutputDirectory'] = os.path.expanduser(self.output_directory)
        write_configuration(self.preferences)
        self.focus_set()
        self.prefix_entry.focus_set()

    def search_by_prefix(self):
        prefix = self.get_prefix().lower()
        found_files = []
        if len(prefix) < 1:
            return
        for root, dirs, files in os.walk(os.path.expanduser(self.input_directory)):
            if prefix != self.get_prefix().lower():
                return
            for name in files:
                name = name.lower()
                if name.startswith(prefix) and name[-4:] in (".pdf", ".mp3"):
                    found_files.append(os.path.join(root, name))

        # stop threads
        if prefix != self.get_prefix().lower():
            return False

        self.found_files = found_files
        return True

    def get_output_filename(self):
        return os.path.join(
            os.path.expanduser(self.output_directory),
            self.output_filename.get()
        )

    def process_pdf_files(self):
        entries = self.selected_files_list.get_list()

        if not entries:
            return

        if entries[0].endswith(".pdf"):
            self.merge_pdf_files()
        elif entries[0].endswith(".mp3"):
            self.merge_mp3_files()

        self.global_status_lbl.configure(
            text=f"Archivos unidos en {self.get_output_filename()}"
        )
        self.after(4000, lambda: self.global_status_lbl.configure(text=f"Listo para unir"))

    def merge_pdf_files(self):
        entries = self.selected_files_list.get_list()

        merger = PdfFileMerger()

        for entry in entries:
            merger.append(entry)

        merger.write(self.get_output_filename())
        merger.close()

    def merge_mp3_files(self):
        number_of_files = len(self.selected_files_list.get_list())
        list_of_files = " -i " + " -i ".join([f"'{p}'" for p in self.selected_files_list.get_list()])
        audio_tracks = "".join([f"[{i}:a]" for i in range(number_of_files)])
        output_filename = self.get_output_filename().replace(".pdf", ".mp3")

        cmd = f"ffmpeg -y {list_of_files} -filter_complex {audio_tracks}concat=n={number_of_files}:v=0:a=1 {output_filename}"
        print(cmd)

        os.system(cmd)

    def reset_entries(self):
        self.prefix_entry.delete(0, tk.END)
        self.list_files.clear()
        self.selected_files_list.clear()
        self.prefix_entry.focus_set()
        self.merge_pdf_btn.config(state=tk.DISABLED)

if __name__ == "__main__":
    app = App()
    app.title("Unir archivos")
    app.mainloop()
