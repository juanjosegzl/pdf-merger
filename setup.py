from distutils.core import setup # Need this to handle modules
import py2exe

import configparser
import sys
import os
import threading
import tkinter as tk
from tkinter import filedialog

from PyPDF2 import PdfFileMerger

setup(
    options={'py2exe': {'bundle_files': 1, 'compressed': True}},
    windows=['main.py'],
    zipfile=None
)
